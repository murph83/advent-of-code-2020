import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Advent of Code 2020 Day 4
 * <a href="https://adventofcode.com/2020/day/4" target="_top">https://adventofcode.com/2020/day/4<a>
 * <p>
 * The hard part about this, initially, is parsing passports in a way that doesn't make me sad. I have not been successful. Nested String.split() for loops make me sad. I've gone back and changed the validators to use an enum because it's 2004 and Java 5 is super cool. It implements Predicate<> because it's 2014 and we're sick of importing Guava to get functional interfaces.
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day4 {

    public static void main(String... args) throws Exception {
        // read the input
        Path input = Paths.get("./Day4.input");
        String batch = Files.readString(input);

        // parse out the passports
        List<Map<String, String>> passports = new ArrayList<>();
        for(String batchEntry : batch.split("\\n[\\n]+")){
            // split the batch on empty lines
            
            Map<String, String> passport = new HashMap<String, String>();
            for(String field : batchEntry.split("\\s+")){
                // split fields on whitespace

                String[] kv = field.split(":");
                passport.put(kv[0],kv[1]);
            }
            passports.add(passport);
        }

        // part 1, all fields must be present
        long partOneCount = passports.stream()
            .filter(passport -> Field.names().allMatch(passport::containsKey))
            .count();
        System.out.printf("Part 1 count of valid passports: %d%n", partOneCount);

        // part 2, all fields must validate
        long partTwoCount = passports.stream()
            .filter(passport -> Field.stream().allMatch(f -> f.test(passport)))
            .count();
        System.out.printf("Part 2 count of valid passports: %d%n", partTwoCount);
    }
    
    // Part 2 validation rules. Here be Dragons.

    // helper function because we're testing "between-ness" a lot
    private static final Predicate<String> between(final int min, final int max){
        return (s) -> {
            int i = Integer.parseInt(s);
            return i >= min && i <= max;
        };
    }

    // a year must be four digits; at least min and at most max
    private static boolean isYearBetween(String year, int min, int max){
        return Pattern.compile("\\d{4}").asMatchPredicate().and(between(min, max)).test(year);
    }

    // the fields and their validators. use an enum because it's 2007 and Java 5 is super cool. Implement Predicate<> because it's 2014 and we're sick of importing Guava to get functional interfaces.
    private static enum Field implements Predicate<Map<String, String>>{
        byr { // birth year must be four digits; at least 1920 and at most 2002
            boolean testInternal(String byr){
                return isYearBetween(byr, 1920, 2002);
            }
        },
        iyr { // issue year must be four digits; at least 2010 and at most 2020
            boolean testInternal(String iyr){
                return isYearBetween(iyr, 2010, 2020);
            }
        },
        eyr { // expiration year must be four digits; at least 2020 and at most 2030
            boolean testInternal(String eyr){
                return isYearBetween(eyr, 2020, 2030);
            }
        },
        hgt { // height is a number followed by either cm or in
            boolean testInternal(String hgt){
                Matcher m = Pattern.compile("(\\d+)(cm|in)").matcher(hgt);
                if (!m.matches()) {
                    return false;
                }
        
                // if 'cm' the number must be at least 150 and at most 193, if 'in' the number must be at least 59 and at most 76
                Predicate<String> range = "cm".equals(m.group(2)) ? between(150, 193) : between(59, 76);
                
                return range.test(m.group(1));
            }
        },
        hcl { // a # followed by exactly six characters 0-9 or a-f
            boolean testInternal(String hcl){
                return Pattern.matches("#[0-9a-f]{6}", hcl);
            }
        },
        ecl { // exactly one of: amb blu brn gry grn hzl oth
            boolean testInternal(String ecl){
                return Pattern.matches("amb|blu|brn|gry|grn|hzl|oth", ecl);
            }
        },
        pid { // a nine digit number, including leading zeros
            boolean testInternal(String pid){
                return Pattern.matches("\\d{9}", pid);
            }
        };

        abstract boolean testInternal(String field);
        public boolean test(Map<String, String> passport){
            return passport.containsKey(name()) && testInternal(passport.get(name()));
        }

        // syntactic sugar
        public static EnumSet<Field> all = EnumSet.allOf(Field.class);

        public static Stream<Field> stream(){
            return all.stream();
        }

        public static Stream<String> names(){
            return stream().map(Field::name);
        }

    }
}
