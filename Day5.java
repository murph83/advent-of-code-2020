import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Advent of Code 2020 Day 5
 * <a href="https://adventofcode.com/2020/day/5" target="_top">https://adventofcode.com/2020/day/5<a>
 * <p>
 * 
 * Boarding Passes are just a replacement cypher for binary numbers. Convert back; sort and search.
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day5 {

    public static void main(String... args) throws Exception{
        // Load in boarding passes, convert to a sorted array of integers
        Path input = Paths.get("./Day5.input");
        List<String> boardingPasses = Files.readAllLines(input);
        int[] seatIds = boardingPasses.stream().mapToInt(Day5::boardingPassToSeatId).sorted().toArray();

        // part 1, find the highest seat id (last number in the sorted array)
        int maxSeatId = seatIds[seatIds.length - 1];

        System.out.printf("Part 1/ Highest Seat ID: %d%n", maxSeatId);

        // part 2, find my seat by stepping through two at a time
        int mySeat = 0;
        for(int i = 0; i < seatIds.length; i+=2){
            // if the seats have a gap > 1, mine is the seat between
            if(seatIds[i+1] - seatIds[i] > 1){
                mySeat = seatIds[i] + 1;
                break;
            }
        }

        System.out.printf("Part 2/ My Seat ID: %d%n", mySeat);
    }

    // F = 0, B = 1, L = 0, R = 1
    private static int boardingPassToSeatId(String boardingPass){
        String binaryString = boardingPass.replace('F', '0').replace('B', '1').replace('L', '0').replace('R', '1');
        int sid = Integer.parseInt(binaryString, 2); //radix: 2
        return sid;
    }
    
}
