import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.BitSet;
import java.util.List;

/**
 * Advent of Code 2020 Day 6
 * <a href="https://adventofcode.com/2020/day/6" target="_top">https://adventofcode.com/2020/day/6<a>
 * <p>
 * 
 * More fun with BitSets. In part 1 we OR the members of a group together (though I don't actually use OR. Just set the bits of each person on top of the last). In part 2 we AND the members of a group together. This time, I actually do create a new BitSet. This whole thing would be better as a Map/Reduce implementation.
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day6 {

    public static void main(String...args) throws Exception {
        Path input = Paths.get("./Day6.input");
        List<String> answers = Files.readAllLines(input);
        answers.add(""); // add on an empty line to make the processing easier

        // part 1 - count the number of questions which *anyone* answers yes
        long anyone = 0;
        BitSet anyoneAnswers = new BitSet();
        for(String person : answers){
            if(person.isBlank()){
                // end of a group, add the number of flagged answers to the running total and reset.
                anyone += anyoneAnswers.cardinality();
                anyoneAnswers.clear();
            }

            for(char c: person.toCharArray()){
                anyoneAnswers.set(c);
            }
        }

        System.out.printf("Part 1 sum: %d%n", anyone);

        // part 2 - count the number of questions which *everyone* answers yes
        long everyone = 0;
        BitSet everyoneAnswers = new BitSet();
        boolean newGroup = true;
        for(String person : answers){
            if(person.isBlank()){
                // end of a group, add the number of flagged answers to the running total and reset.
                everyone += everyoneAnswers.cardinality();
                everyoneAnswers.clear();
                newGroup = true;
                continue;
            }

            if(newGroup){ 
                // set the flags to match the first person
                for(char c: person.toCharArray()){
                    everyoneAnswers.set(c);
                }
                newGroup = false;
            } else {
                // for every other person, unset missing flags
                BitSet individualAnswers = new BitSet();
                for(char c: person.toCharArray()){
                    individualAnswers.set(c);
                }
                everyoneAnswers.and(individualAnswers);
            }
        }

        System.out.printf("Part 2 sum: %d%n", everyone);
    }
    
}
