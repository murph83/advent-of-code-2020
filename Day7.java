import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Advent of Code 2020 Day 7
 * <a href="https://adventofcode.com/2020/day/7" target="_top">https://adventofcode.com/2020/day/7<a>
 * <p>
 * 
 * Directed Acyclic Graphs are all the rage nowadays. I completely rewrote the graph, but I'm not sure it's better.
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day7 {

    public static void main(String... args) throws Exception {
        Path input = Paths.get("./Day7.input");
        List<String> rules = Files.readAllLines(input);

        BagGraph graph = new BagGraph().buildFrom(rules);

        // part 1 - traverse the graph, counting the number of bags we hit
        Deque<String> stack = new ArrayDeque<>();
        Set<String> visited = new HashSet<>();
        stack.push("shiny gold");
        while(!stack.isEmpty()){
            String node = stack.pop();
            graph.reverse().adjacencyList.get(node).stream().map(e -> e.contained).filter(visited::add).forEach(stack::push);
        }

        System.out.printf("Part 1 number of bags that can hold at least one shiny gold bag: %d%n", visited.size());

        // part 2 - number of bags required inside shiny gold bag
        long bags = containedBagCount(graph, "shiny gold");

        System.out.printf("Part 2 number of bags inside one shiny gold bag: %d%n", bags);
        
    }

    // it's recursive, but I don't care
    private static int containedBagCount(BagGraph graph, String bag){
        return graph.adjacencyList.get(bag).stream().mapToInt(e -> e.cardinality * (1 + containedBagCount(graph, e.contained))).sum();
    }

    private static class BagGraph {
        Map<String, LinkedList<Edge>> adjacencyList;

        BagGraph buildFrom(List<String> rules){
            adjacencyList = new HashMap<>(rules.size());
            for (String rule : rules){
                String[] bagRule = rule.split(" bags contain ");
                
                String container = bagRule[0];
                adjacencyList.put(container, new LinkedList<>());
                Arrays.stream(bagRule[1].split(",\\s"))
                    .filter(contained  -> !"no other bags.".equals(contained))
                    .forEach(target -> buildEdge(container, target));
            }
            return this;
        }

        BagGraph buildEdge(String source, String destination){
            String[] bagDefinition = destination.split(" ");
            String contained = bagDefinition[1] + " " + bagDefinition[2];
            int cardinality = Integer.parseInt(bagDefinition[0]);
            
            addEdge(source, contained, cardinality);
            
            return this;
        }

        BagGraph addEdge(String container, String contained, int cardinality){
            adjacencyList.get(container).addFirst(new Edge(container, contained, cardinality));
            return this;
        }

        BagGraph reverse(){
            BagGraph reverse = new BagGraph();
            reverse.adjacencyList = new HashMap<>(adjacencyList.size());
            adjacencyList.keySet().stream().forEach(b -> reverse.adjacencyList.put(b, new LinkedList<>()));
            for (String bag : adjacencyList.keySet()){
                adjacencyList.get(bag).stream().forEach(e -> reverse.addEdge(e.contained, e.container, e.cardinality));
            }

            return reverse;
        }

        private class Edge {
            String container;
            String contained;
            int cardinality;

            Edge(String container, String contained, int cardinality){
                this.container = container;
                this.contained = contained;
                this.cardinality = cardinality;
            }
        }
    }
}


