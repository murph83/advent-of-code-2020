import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Advent of Code 2020 Day 2
 * <a href="https://adventofcode.com/2020/day/2" target="_top">https://adventofcode.com/2020/day/2<a>
 * <p>
 * Hooray Regex!
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day2 {

    public static void main(String... args) throws Exception {
        // load the Policy/Password entries
        Path input = Paths.get("./Day2.input");
        List<String> entries = Files.readAllLines(input);

        // named groups allow for easier fetching. 
        Pattern entryPattern = Pattern.compile("(?<x>\\d+)-(?<y>\\d+) (?<c>\\w): (?<p>\\w+)");

        // part 1 - c must occur between x and y times in p
        long partOneCount = entries.stream()
            .map(entryPattern::matcher)
            .filter(m -> m.matches() && isValidByOldPolicy(m))
            .count();

        System.out.printf("Number of valid passwords under part1 policy: %d%n", partOneCount);

        // part 2 - c must occur at either index x XOR index y in p
        long partTwoCount = entries.stream()
            .map(entryPattern::matcher)
            .filter(m -> m.matches() && isValidByNewPolicy(m))
            .count();

        System.out.printf("Number of valid passwords under part2 policy: %d%n", partTwoCount);
    }

    // c must occur between x and y times in p
    private static boolean isValidByOldPolicy(Matcher entry){
        int min = Integer.parseInt(entry.group("x"));
        int max = Integer.parseInt(entry.group("y"));
        char letter = entry.group("c").charAt(0);
        String password = entry.group("p");

        long count = password.chars().filter(c -> c == letter).count();

        return count >= min && count <= max;
    }

    // c must occur at either index x XOR index y in p
    private static boolean isValidByNewPolicy(Matcher entry){
        // remember, the indexes in the policies are off by 1
        int xindex = Integer.parseInt(entry.group("x")) - 1;
        int yindex = Integer.parseInt(entry.group("y")) - 1;
        char letter = entry.group("c").charAt(0);
        String password = entry.group("p");

        return password.charAt(xindex) == letter ^ password.charAt(yindex) == letter;
    }
}