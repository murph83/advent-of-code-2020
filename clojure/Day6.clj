(ns Day6
  (:require [clojure.java.io]
            [clojure.string :refer [blank?]]))

(defn str-to-bits
  "Convert a string to a bitset"
  [s]
  (->> (seq s)
       (map int)
       (map (partial bit-set 0))
       (reduce bit-or)))

(defn -main
  "This is part 1. Part 2 is exactly the same, except with bit-and instead of bit-or in the group reduction"
  []
  (with-open [rdr (clojure.java.io/reader "./Day6.input")]
    (->> (line-seq rdr) ; read in the input
         (partition-by blank?) ; split on empty lines
         (take-nth 2) ; skip empty lines
         (map (comp (partial reduce bit-or) (partial map str-to-bits))) ; map-reduce each group via bitwise-or
         (map (comp #(.bitCount %) biginteger)) ; count the bits
         (reduce +) ; add 'em up
         (println))))
    