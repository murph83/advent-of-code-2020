# Advent of Code 2020

I got sucked into this. Thanks Florian.

## Running
Each Day is self-contained. You should be able to `java DayX.java --source 11` each one to run it.

## Implementation notes
This is not good code. It's quick and dirty problem solving. Optimization for performance is only to allow me to finish each problem before my tea gets cold. But hopefully it's not too embarassing to look at.

## Day notes
### [Day 1](Day1.java)
We're not doing anything fancy here: a nested loop through the entries. Since we know there's only one right answer, we can break out of the loop as soon as we find it.

### [Day 2](Day2.java)
Man, I wish Java had a tuple primitive and parameter destructuring. I ended up reworking this one to use Regex, but there's still a ton of massaging I need to do to get the various policy inputs into the right data types.

### [Day 3](Day3.java)
Fun with `BitSet`s. By converting the trees and the toboggan path into `BitSet`s, we can AND them together to see where the "encounters" are. For part 2, we need to be able to pass in different slopes for the toboggan to calculate our results. We do this with a couple of helper classes.

### [Day 4](Day4.java)
The hard part about this, initially, is parsing passports in a way that doesn't make me sad. I have not been successful. Nested `String.split()` in `for` loops make me sad. I've gone back and changed the validators to use an `enum` because it's 2004 and Java 5 is super cool. It implements `Predicate<>` because it's 2014 and we're sick of importing Guava to get functional interfaces.

### [Day 5](Day5.java)
This is perhaps the longest walk to "which is the highest binary number?" I've ever taken. Replacing F = 0, B = 1, L = 0, R = 1 in the example yields

```
FBFBBFFRLR
0101100101 -> 357
```
So we need to replace digits and call `.parseInt` with a radix of 2.

### [Day 6](Day6.java)
More fun with `BitSet`s. In part 1 we OR the members of a group together (though I don't actually use OR. Just set the bits of each person on top of the last). In part 2 we AND the members of a group together. This time, I actually do create a new `BitSet`. I think this whole thing would be better as a Map/Reduce implementation.

To give that a try, I switched to Clojure. You can see the results of Part 1 in [Day6.clj](clojure/Day6.clj). I've always been a fan of Clojure, and this problem seems tailor-made for the language. I also tried a M/R implementation in Java; you can see the results in [Day6MR.java](Day6MR.java). I don't think it actually turned out any better than the original version. It did highlight the different things that Java and Clojure are good at though. 

Clojure is really great at partitioning the input into a clean list of lists:
```clojure
(with-open [rdr (clojure.java.io/reader "./Day6.input")]
    (->> (line-seq rdr) ; read in the input
         (partition-by blank?) ; split on empty lines
         (take-nth 2) ; skip empty lines
```
Note that since `line-seq`, `partition-by`, and `take-nth` all produce lazy sequences, we haven't actually read anything into memory yet! The Buffered reader is open, but as long as what I need to do downstream is equally lazy, I may never read the whole file.

vs
```java
Path input = Paths.get("./Day6.input");
List<String> answers = Files.readAllLines(input);
answers.add(""); // list terminator

// find the indices of empty lines and split the input into sublists
List<List<String>> groups = new ArrayList<>();
int[] groupIndices = IntStream.range(0, answers.size()).filter(i -> answers.get(i).isEmpty()).toArray();
int previousIndex = 0;
for(int index : groupIndices){
    groups.add(answers.subList(previousIndex, index)
    previousIndex = index + 1;
}
```
This may not be a totally fair comparison. But the point is the algorithm I had in mind was "Split the list of inputs into sub lists based on the presence of empty lines"; I put about the same amount of thought into each implementation, and the Clojure version is 4 lines of code.

On the flip side, I actually prefer the Java code I came up with to map a string to a bit set.
```clojure
(defn str-to-bits
  "Convert a string to a bit set"
  [s]
  (->> (seq s)
       (map int)
       (map (partial bit-set 0))
       (reduce bit-or)))
```
vs
```java
// convert a String to a BitSet
private static BitSet personToBitSet(String person){
    BitSet bs = new BitSet();
    person.chars().forEach(c -> bs.set(c));
    return bs;
}
```
This just feels tidier to me. Mostly because Java still lets me treat `char`s as `int`s. Apparently Clojure forgot how to do that.

### [Day 7](Day7.java)
I pretty quickly zeroed in on a Directed Acyclic Graph to track the bag relationships. My first implementation was ugly as hell. Look at the commit history if you want to see it.

After completely rewriting it, the second implementation is still ugly as hell. This is why people use libraries. If instead of implementing my own graph, I had used a nice java graph library, this would have taken five minutes and been a handful of lines of code.

### [Day8](Day8.java)
It is a real struggle to resist the temptation to improve my boundry checking when working these problems. For example: the only reason why the operation flip logic in `BootLoader` is safe is because it implicitly knows that it'll never be sent an index for an 'acc' instruction. This makes my brain twitch, but there's not much value in making the code more robust, becuase it's not going to be used for anything else.

### [Day 9](Day9.java)
Lots of brute-force loops in this one. The fundamental approach is the same in both parts. 
1. construct a sliding window 
2. test if the window conforms to a predicate 
3. if not, advance the window
I use a `LinkedList` for the window because it's both a queue and a list. Handy for sliding the window forward and then iterating over the contents to calculate a variety of sums for the tests.

Once again, I went back to clean up a few things. I like how the `SlidingWindow` class turned out. It nicely encapsulates the search algorithm separate from the tests. It particularly bothered me that my original implementations for part 1 and part 2 had the same structure, but the if/else logic to either break out of the loop or advance the window was reversed. Now it's consistent *and* reusable.

I'm not as thrilled with what this meant for the tests themselves. If I was going to reuse the sliding window logic, I wanted to have a consistent approach to its use. I wanted each test to be a method reference, or a lambda, or a full `BiPredicate` implementation. Previously, the logic for part 1 was in a separate method because it was two more `for` loops, and going three deep on that was not ok in the body of `main`. One point for method references. Unfortunately, the logic for part 2 closes over the result from part 1, so it's not possible to use a method reference without wrapping the result into an object with the test method on it. Boilerplate for `BiPredicate` classes, here I come. In order to avoid that fate I got clever with the implementation for the part 1 test, inverting the logic and deploying nested `IntStream`s to replace the loops.

```java
// return true if any two numbers in window sum to next
private static boolean isValidNextNumber(List<Long> window, Long next){
    for (int i = 0; i < window.size() - 1 ; i++){
        for (int j = i + 1; j < window.size(); j++){
            if (next == window.get(i) + window.get(j)){
                return true;
            }
        }
    }
    return false;
}
```

became

```java
BiPredicate<List<Long>, Long> nextIsNotSum = 
            (window, next) -> IntStream.range(0, window.size()).allMatch(i -> IntStream.range(i + 1, window.size()).noneMatch(j -> window.get(i) + window.get(j) == next));

```

While I may never know if I did the right thing, History will judge me for my actions this day.

### [Day 10](Day10.java)
I got stumped on part 2 for a while. Ultimately, I went to reddit for some guidance, and came back with a pretty decent approach.
