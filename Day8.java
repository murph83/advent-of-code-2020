import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Advent of Code 2020 Day 8
 * <a href="https://adventofcode.com/2020/day/8" target="_top">https://adventofcode.com/2020/day/8<a>
 * <p>
 * 
 * I keep wanting to over-engineer these things.
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day8 {

    public static void main(String... args) throws Exception{
        Path input = Paths.get("./Day8.input");
        List<String> program = Files.readAllLines(input);

        // Load in the program
        List<Instruction> memory = new ArrayList<>(program.size());
        for(String s : program){
            String[] instruction = s.split(" ");
            memory.add(new Instruction(instruction[0], Integer.parseInt(instruction[1])));
        }

        // part 1 - run until loop. print accumulator
        BootLoader broken = new BootLoader(memory);
        broken.run();
        System.out.printf("Part 1 - accumulator value before loop: %d%n", broken.accumulator);

        // part 2 - working backwards, change one instruction until everything works.
        BootLoader fixed = null;
        for(int i = memory.size() - 1; i >=0; i--){
            // for each operation that isn't an 'acc', try flipping it
            if(!memory.get(i).operation.equals("acc")){
               fixed = new BootLoader(memory, i);

               // if the run completes normally, we've found the corrupted operation
               if(fixed.run()) break;
            }
        }

        System.out.printf("Part 1 - accumulator value after termination: %d%n", fixed.accumulator);
        
    }

    // an instruction has an operation and an argument
    private static class Instruction {
        String operation;
        int argument;

        Instruction(String operation, int argument){
            this.operation = operation;
            this.argument = argument;
        }
    }

    // wrap the execution logic into a BootLoader class so we can run it multiple times
    private static class BootLoader {
        int accumulator = 0;
        List<Instruction> rom;
        int flipInstruction;

        BootLoader(List<Instruction> memory){
            this(memory, -1);
        }

        // flipInstruction is the index of a single instruction to flip from nop to jmp or jmp to nop
        BootLoader(List<Instruction> memory, int flipInstruction){
            this.rom = Collections.unmodifiableList(memory);
            this.flipInstruction = flipInstruction;
        }

        boolean run(){
            Set<Integer> visited = new HashSet<>();
            int instructionPointer = 0;
            // terminate if we hit an instruciton loop, or if the program executes to the end of memory
            while(!visited.contains(instructionPointer) && instructionPointer < rom.size()){
                visited.add(instructionPointer);
                String operation = rom.get(instructionPointer).operation;

                // flip 'jmp' to 'nop' or 'nop' to 'jmp' if a flip index is set
                if(instructionPointer == flipInstruction){
                    operation = "jmp".equals(operation) ? "nop" : "jmp";
                }

                // execute the operation
                switch(operation){
                    case "acc":
                        accumulator += rom.get(instructionPointer).argument;
                        instructionPointer++;
                        break;
                    case "jmp":
                        instructionPointer += rom.get(instructionPointer).argument;
                        break;
                    case "nop":
                        instructionPointer++;
                        break;
                }
            }

            // return true if execution completed normally
            return instructionPointer == rom.size();
        }

    }

    
}
