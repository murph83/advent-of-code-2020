import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Advent of Code 2020 Day 9
 * <a href="https://adventofcode.com/2020/day/9" target=
 * "_top">https://adventofcode.com/2020/day/9<a>
 * <p>
 * 
 * Lots of brute-force loops in this one. The fundamental approach is the same
 * in both parts. 1) construct a sliding window 2) test if the window conforms
 * to a predicate 3) if not, advance the window.
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day9 {

    public static void main(String... args) throws Exception {
        Path input = Paths.get("./Day9.input");
        List<Long> XMAS = Files.readAllLines(input).stream().map(Long::parseLong).collect(Collectors.toList());

        // part 1 - find the first number which is not the sum of two numbers from the
        // preceding 25
        int preambleLength = 25;
        SlidingWindow slidingWindow = new SlidingWindow(XMAS, preambleLength);
        BiPredicate<List<Long>, Long> nextIsNotSum = 
            (window, next) -> IntStream.range(0, window.size()).allMatch(i -> IntStream.range(i + 1, window.size()).noneMatch(j -> window.get(i) + window.get(j) == next));

        Optional<Long> invalidResult = slidingWindow.advanceUntil(nextIsNotSum);
        long firstInvalid = invalidResult.get();

        System.out.printf("Part 1 - first invalid number: %d%n", firstInvalid);

        // part 2 - find a contiguous list of 2+ numbers that sum to the invalid number
        BiPredicate<List<Long>, Long> condition = (window, n) -> window.stream().reduce(0L, Long::sum) == firstInvalid;
        SlidingWindow contiguous = null;
        boolean found = false;
        
        for (int i = 2; i < XMAS.size() && !found; i++) {
            contiguous = new SlidingWindow(XMAS, i);
            Optional<Long> testResult = contiguous.advanceUntil(condition);
            found = testResult.isPresent();
        }

        // encryption weakness is the sum of smallest and largest numbers in this
        // contiguous range
        Collections.sort(contiguous.window);
        long encryptionWeakness = contiguous.window.getFirst() + contiguous.window.getLast();
        System.out.printf("Part 2 - encryption weakness: %d%n", encryptionWeakness);
    }

    private static class SlidingWindow {
        List<Long> list;
        LinkedList<Long> window;
        int windowSize;

        SlidingWindow(List<Long> list, int windowSize) {
            this.list = list;
            this.window = new LinkedList<>(list.subList(0, windowSize));
            this.windowSize = windowSize;
        }

        Optional<Long> advanceUntil(BiPredicate<List<Long>, Long> condition) {
            for (Long n : list.subList(windowSize, list.size())) {
                if (condition.test(window, n)) {
                    return Optional.of(n);
                } else {
                    // advance the window
                    window.remove();
                    window.add(n);
                }
            }

            return Optional.empty();

        }

    }

}