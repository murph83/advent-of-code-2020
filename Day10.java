import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Advent of Code 2020 Day 10
 * <a href="https://adventofcode.com/2020/day/10" target="_top">https://adventofcode.com/2020/day/10<a>
 * <p>
 * 
 * This one was tricky. Had to go to reddit for a clue for part 2. But the trick with tribonacci numbers is super cool.
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day10 {

    public static void main(String... args) throws Exception{
        // read the input, add 0 and max +3 to the list of joltages
        Path input = Paths.get("./Day10.input");
        List<Integer> adapters = Files.readAllLines(input).stream().map(Integer::parseInt).collect(Collectors.toList());
        adapters.add(0);
        Collections.sort(adapters);
        adapters.add(adapters.get(adapters.size() - 1) + 3);


        // for part 1 we're going to count the number of 1- and 3-joltage gaps. At the same time, we split into groups at the 3-joltage gaps
        int ones = 0;
        int threes = 0;
        List<List<Integer>> groups = new ArrayList<>();
        int groupIndex = 0;
        int i = 1;
        for(; i < adapters.size(); i++){
            int gap = adapters.get(i) - adapters.get(i - 1);
            if(gap ==1){
                ones++;
            } else if (gap == 3){
                threes++;
                groups.add(adapters.subList(groupIndex, i));
                groupIndex = i;
            }
        }

        System.out.printf("Part 1 - (ones) * (threes): %d%n", ones * threes);

        // each separate group can be arranged in a number of ways equal to Tribonacci(groupSize) (thanks reddit). Multiply them together for the total number of ways to arrange the adapters.
        int[] trib = {0,1,1,2,4,7};
        long arrangements = groups.stream()
            .mapToLong(g -> trib[g.size()])
            .reduce(1L, Math::multiplyExact);

        System.out.printf("Part 2 - number of arrangements: %d%n", arrangements);

    }
    
}
