import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Advent of Code 2020 Day 3
 * <a href="https://adventofcode.com/2020/day/3" target="_top">https://adventofcode.com/2020/day/3<a>
 * <p>
 * Fun with BitSets. By converting the trees and the toboggan path into BitSets, we can just AND them together to see where the "encounters" are. For part 2, we need to be able to pass in different slopes for the toboggan to calculate our results. We do this with a couple of helper classes.
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day3 {

    public static void main(String... args) throws Exception {
        // read the input
        Path input = Paths.get("./Day3.input");
        List<String> lines = Files.readAllLines(input);

        // load the map
        Map map = new Map(lines);
        
        // part 1
        // create a toboggan path with slope 3,1
        Toboggan toboggan = new Toboggan(3, 1);

        // check its path through the trees
        BitSet run = map.run(toboggan);
        int hitTrees = run.cardinality();

        // print results
        System.out.printf("Trees encountered in part 1: %d%n", hitTrees);

        // part 2
        // for each slope, send down a toboggan
        List<Toboggan> toboggans = Arrays.asList(
            new Toboggan(1, 1),
            new Toboggan(3, 1),
            new Toboggan(5, 1),
            new Toboggan(7, 1),
            new Toboggan(1, 2));

        // collect the "encounters" and multiply them together (use Math to avoid overflow)
        long product = 1;
        for(Toboggan t : toboggans){
            int encounters = map.run(t).cardinality();
            product = Math.multiplyExact(product, encounters);
        };
        
        // print results
        System.out.printf("Product of trees encountered in part 2: %d%n", product);
        
    }

    private static class Map {
        final BitSet trees;
        final int height;
        final int width;
        final int size;

        Map(List<String> input){
            // calculate the size of the map
            height = input.size();
            width = input.get(0).length();
            size = height * width;

            // flatten the map into a BitSet with bits set at the position of trees
            String linearMap = input.stream().collect(Collectors.joining());
            trees = new BitSet(size);
            for(int i = 0; i < linearMap.length(); i++){
                if(linearMap.charAt(i) == '#'){
                    trees.set(i);
                }
            }
        }

        // a Map.Traverser is just a Toboggan, but this is a little bit of syntactic sugar to avoid a circular dependency between Toboggan and Map
        private interface Traverser extends Function<Map, BitSet>{};

        BitSet run(Traverser toboggan){
            // get the toboggan's path through this map
            BitSet path = toboggan.apply(this);

            // AND the path with the trees
            path.and(trees);

            return path;
        }
    }

    private static class Toboggan implements Map.Traverser{
        final int right;
        final int down;

        Toboggan(int... slope){
            this.right = slope[0];
            this.down = slope[1];
        }

        public BitSet apply(Map map) {
            // generate a BitSet with bits set at the positions the toboggan will hit
            BitSet path = new BitSet(map.size);
            for(int i = 0, x = 0; i < map.height; i+= down, x = (x + right) % map.width){
                path.set((map.width * i) + x);
            }

            return path;
        }
        
    }

    
}