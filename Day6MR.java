import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Advent of Code 2020 Day 6
 * <a href="https://adventofcode.com/2020/day/6" target="_top">https://adventofcode.com/2020/day/6<a>
 * <p>
 * 
 * Took another swing at Day 6 with Map/Reduce
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day6MR {

    public static void main(String...args) throws Exception {
        Path input = Paths.get("./Day6.input");
        List<String> answers = Files.readAllLines(input);
        answers.add(""); // list terminator

        // divide the answers into groups of BitSets
        List<List<BitSet>> groups = new ArrayList<>();
        int[] groupIndices = IntStream.range(0, answers.size()).filter(i -> answers.get(i).isEmpty()).toArray();
        int previousIndex = 0;
        for(int index : groupIndices){
            groups.add(answers.subList(previousIndex, index).stream().map(Day6MR::personToBitSet).collect(Collectors.toList()));
            previousIndex = index + 1;
        }

        // part 1 - sum the groups after ORing them
        int anyone = groups.stream()
            .map(group -> 
                group.stream()
                    .reduce((left, right) -> {left.or(right); return left;})
                    .orElse(new BitSet()))
            .mapToInt(BitSet::cardinality)
            .sum();

        System.out.printf("Part 1 sum: %d%n", anyone);

        // part 2 - sum the groups after ANDing them
        int everyone = groups.stream()
            .map(group -> 
                group.stream()
                    .reduce((left, right) -> {left.and(right); return left;})
                    .orElse(new BitSet()))
            .mapToInt(BitSet::cardinality)
            .sum();

        System.out.printf("Part 2 sum: %d%n", everyone);

    }

    // convert a String to a BitSet
    private static BitSet personToBitSet(String person){
        BitSet bs = new BitSet();
        person.chars().forEach(c -> bs.set(c));
        return bs;
    }
}
