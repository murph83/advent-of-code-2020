import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Advent of Code 2020 Day 1 
 * <a href="https://adventofcode.com/2020/day/1" target="_top">https://adventofcode.com/2020/day/1<a>
 * <p>
 *  
 * We're not doing anything fancy here. Just a nested loop through the entries. Since we know there's only one right answer, we can break out of the loop as soon as we find it
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day1 {

    public static void main(String... args) throws Exception {
        // parse the input into an array of integers
        Path input = Paths.get("./Day1.input");
        final int[] entries = Files.readAllLines(input).stream().mapToInt(Integer::parseInt).toArray();

        // part 1 - sum of two numbers == 2020
        int i, j;
        i = j = 0;
        outerloop: 
        for (; i < entries.length; i++) {
            for (j = i; j < entries.length; j++) {
                if (entries[i] + entries[j] == 2020)
                    break outerloop;
            }
        }

        System.out.printf("Part 1 result: %d%n", entries[i] * entries[j]);

        // part 2 - sum of three numbers == 2020
        int x,y,z;
        x = y = z = 0;
        outerloop: 
        for (; x < entries.length; x++) {
            for (y = x; y < entries.length; y++) {
                for (z = y; z < entries.length; z++){
                    if (entries[x] + entries[y] + entries[z] == 2020)
                        break outerloop;
                }
            }
        }

        System.out.printf("Part 1 result: %d%n", entries[x] * entries[y] * entries[z]);

    }
}